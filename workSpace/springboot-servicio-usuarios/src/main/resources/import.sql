INSERT INTO usuarios (username, password, enable, nombre, apellido, email) VALUES ('andres', '$2a$10$VeBXgp7itqjxuilyKkkWBe4ffoWewfaOwBanMcshM9g4lfzbUwQHm', true, 'Andres', 'Guzman', 'correo1@correo.cl');
INSERT INTO usuarios (username, password, enable, nombre, apellido, email) VALUES ('admin', '$2a$10$1slk.YUqbwcKeOYhC5DFKOeumqxraqzvR74YsVDtzY1d25zU/g6hq', true, 'Jhon', 'Doe', 'correo2@correo.cl');

INSERT INTO roles (nombre) VALUES ('ROLE_USER')
INSERT INTO roles (nombre) VALUES ('ROLE_ADMIN')

INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (1, 1)
INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (2, 2)
INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (2, 1)