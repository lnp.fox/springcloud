package com.formacionbdi.springboot.app.usuarios.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.formacionbdi.springboot.app.commons.usuarios.models.entity.Usuario;

@RepositoryRestResource(path = "usuarios")
public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long> {

	/*
	 * Este forma de hace la consulta es propia de Spring JPA se debe investigar en
	 * el sitio de Spring las diferentes reglas de metodos.
	 * Con @RestResource podemos
	 * sobre escribir el path para que al momento de llamarlo desde el endpoint se
	 * llame como queramos, lo mismo con el @Param /usuarios/search/findUsername?username=admin
	 * si no ocupamos @RestResource quedarìa como /usuarios/search/findByUsername?username=admin
	 */
	@RestResource(path = "findUsername")
	public Usuario findByUsername(@Param("username") String username);

	/*
	 * Este forma es manual, se puede hacer la query directa a JPA
	 */
	@Query("select u from Usuario u where u.username=?1")
	public Usuario obtenerPorUsername(String username);
}
